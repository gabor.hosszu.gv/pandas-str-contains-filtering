import pandas
df = pandas.read_excel('szavak_final.xlsx', index_col=0)

# itt van megadva, hogy melyik karaktert kell tartalmaznia a dataframe 'szo' oszlopánk
# ahol ez nincs jelen, az a sor törlődik
CHARACTER = "ó"

print(df)
df.drop(df[df["szo"].str.contains(CHARACTER) == False].index, inplace=True)
print(df)
