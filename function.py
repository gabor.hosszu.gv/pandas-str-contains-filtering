import pandas


def filter_for_matching(df: pandas.DataFrame, column: str, chars: str, keep: bool = False) -> pandas.DataFrame:
    """
    A "df" DataFrame "column" oszlopán végighaladva vizsgálja a sorokat, hogya "chars" string összes karaktere szerelep-e bennük.
    A "keep" függvényében ezeket tartja meg, vagy ezeket törli.

    Args:
        df (pandas.DataFrame): a módosítanivaló Dataframe
        column (str): az vizsgálandó oszlop elnevezése a DataFrame-ben
        chars (list[str]): a szükséges karaktereket tartalmazó karakterlánc
        keep (bool): Ha False, törli a sorokat ahol nincs egyezés. Ha True törli a sorokat ahol van egyezés.

    Returns:
        pandas.DataFrame: A szűrt Dataframe
    """
    for char in chars:
        df.drop(df[df[column].str.contains(char) == keep].index, inplace=True)
    return df


# Példa a használatra
# csak azokat a sorokat adja vissza ahol bárhol szerepel "v" és "ó"
dataframe = pandas.read_excel('szavak_final.xlsx', index_col=0)
print(dataframe)
dataframe_2 = filter_for_matching(dataframe, "szo", "vó")
print(dataframe_2)
